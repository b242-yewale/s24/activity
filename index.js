let getCube = Math.pow(2, 3); 

console.log(`The cube of 2 is ${getCube}`);

const address = ['Hostel 18', 'IIT-B Campus', 'Powai', 'Mumbai', 'Maharashtra', 'India', '400076'];
const [hostel, area, node, city, state, country, pincode] = address;
console.log(`I live at ${hostel} in ${area}, located in ${node}, ${city}, ${state}, ${country}, Pin: ${pincode}.`)

const animal = {
	petName: 'Lolong',
	waterType: 'Saltwater',
	weight: '1075 kgs',
	length: '20 ft 3 in'
};

const {petName, waterType, weight, length} = animal;

console.log(`${petName} was a ${waterType} crocodile. He weighted at ${weight} with a measurement of ${length}.`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number);
})

let sum = numbers.reduce((prev, cur) => prev + cur);
console.log(sum);

function Dog(name, age, breed){
	this.name = name;
	this.age = age;
	this.breed = breed;
}

let myDog = new Dog('Tom', 5, 'Indian pariah');
console.log(myDog);
